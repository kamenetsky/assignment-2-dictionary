LD = ld
LD_FLAGS = -o
ASM = nasm
ASM_FLAGS = -f elf64 -o
PY = python3

main: main.o lib.o dict.o
	$(LD) -o $@ $^

main.o: main.asm words.inc
	$(ASM) $(ASM_FLAGS) $@ $<

lib.o: lib.asm
	$(ASM) $(ASM_FLAGS) $@ $<

dict.o: dict.asm
	$(ASM) $(ASM_FLAGS) $@ $<

clean:
	rm -rf *.o
	rm -f main

test: main
	$(PY) test.py

.PHONY: clean test

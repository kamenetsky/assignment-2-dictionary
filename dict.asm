section .text

global find_word

%include 'lib.inc'
%define ID 8

find_word:
    push rbx             
    push rbp             
    
.loop:
    push rsi
    push rdi
    add rsi, ID
    call string_equals
    pop rdi
    pop rsi
    test rax, rax
    jnz .found
    mov rsi, [rsi]
    test rsi, rsi
    jnz .loop
    xor rax, rax
    pop rbp              
    pop rbx              
    ret

.found:
    mov rax, rsi
    pop rbp              
    pop rbx              
    ret

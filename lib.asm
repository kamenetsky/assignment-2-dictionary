section .text


global exit
global string_length
global print_string
global print_char
global print_newline
global print_uint
global print_int
global print_hex
global string_equals
global read_char
global read_word
global parse_int
global parse_uint
global string_copy
global read_string
global print_error

 
; Принимает код возврата и завершает текущий процесс
exit: 
    mov rax, 60 ;запись кода системного вызова
    syscall
    ret

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
xor rax, rax
.loop:
    cmp byte [rdi+rax], 0
    je .end
    inc rax
    jmp .loop
.end:
    ret
; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    push rdi
    call string_length
    pop rsi
    mov rdx, rax
    mov rax, 1
    mov rdi, 1
    syscall
    ret

; Принимает код символа и выводит его в stdout
print_char:
    push rdi
    mov rdx, 1     
    mov rsi, rsp 
    pop rdi
    mov rax, 1      
    mov rdi, 1      
    syscall         
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    xor rax, rax
    mov rdi, '\n'
    jmp print_char

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    mov rax, rdi
    push rbx
    mov rbx, 10
    xor rcx, rcx
.push_loop:
    xor rdx, rdx
    div rbx
    add rdx, '0'
    push rdx
    inc rcx
    test rax, rax
    jnz .push_loop
.print_loop:
    pop rdi
    push rcx
    call print_char
    pop rcx
    dec rcx
    test rcx, rcx
    jnz .print_loop
    pop rbx
    ret   

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    xor rax, rax
    cmp rdi,0
    jl .neg 
    jmp print_uint
    
.neg:
    push rdi 
    mov rdi, '-' 
    call print_char
    pop rdi
    neg rdi 
    jmp print_uint
    ret

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
push r15
xor r15, r15
.loop:
    mov dl, byte [rdi + r15]
    cmp dl, byte [rsi + r15]
    jne .not
    inc r15
    test dl, dl
    jne .loop
    mov rax, 1
    pop r15
    ret
.not:
    pop r15
    xor rax, rax
    ret


; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    xor rax, rax
    xor rdi, rdi
    mov rdx, 1
    push rax
    mov rsi, rsp
    syscall
    pop rax
    ret



; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
xor rcx, rcx
.loop:
    push rdi
    push rsi
    push rcx
    call read_char
    pop rcx
    pop rsi
    pop rdi
    cmp rax, ` `
    je .check
    cmp rax, `\t`
    je .check
    cmp rax, `\n`
    je .check
    cmp rax, `\0`
    je .end
    mov [rdi+rcx], rax
    inc rcx
    cmp rcx, rsi
    jl .loop
    xor rax, rax
    ret

.check:
    test rcx, rcx
    jz .loop

.end:   
    mov byte[rdi+rcx], 0
    mov rdx, rcx
    mov rax, rdi
    ret


read_string:
        xor rcx, rcx           
    .loop:
        push rdi                
        push rsi
        push rcx
        call read_char        
        pop rcx               
        pop rsi
        pop rdi
        cmp rax, 0x20         
        je .space
        cmp rax, 0x9
        je .space
        cmp rax, 0xA
        je .newline
        cmp rax, 0              
        je .end
    .continue:    
        mov [rdi + rcx], rax    
        inc rcx                
        cmp rcx, rsi           
        jge .err
        jmp .loop
    .space:
        cmp rcx, 0             
        je .loop                
        jmp .continue
    .newline:
        cmp rcx, 0
        je .loop
        jmp .end
    .err:
        xor rax, rax            
        xor rdx, rdx           
        ret
    .end:
        xor rax, rax           
        mov [rdi + rcx], rax
        mov rax, rdi           
        mov rdx, rcx           
        ret                    

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
xor rax, rax
xor rdx, rdx
xor r10, r10
mov r11, 10

.loop:
    mov r10b, byte[rdi+rdx]
    cmp r10b, `0`
    jl .result
    cmp r10b, `9`
    jg .result
    push rdx
    mul r11
    pop rdx
    sub r10b, `0`
    add rax, r10
    inc rdx
    jmp .loop

.result:
    ret
	



; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    cmp byte[rdi], '-' 	
    je .negative 		
    jmp parse_uint	 	
.negative:
    inc rdi 	
    call parse_uint 	
    neg rax; 		
    inc rdx 		
    ret


; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
xor rax, rax
push rbx
.loop:
    mov bl, byte[rdi+rax]
    mov byte[rsi+rax], bl
    dec rdx
    js .err
    cmp byte[rdi+rax], 0
    inc rax
    jne .loop
    jmp .end
.err:
     xor rax, rax

.end:
     pop rbx
     ret
     
print_error:
    push rdi
    call string_length
    pop rdi
    mov rsi, rdi
    mov rdx, rax
    mov rax, 1 
    mov rdi, 2
    syscall
    ret


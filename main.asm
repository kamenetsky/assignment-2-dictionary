%include "lib.inc"
%include "dict.inc"
%include "words.inc"
%define MAX_LENGTH 256

section .bss
    buff: resb MAX_LENGTH

section .rodata
    err_overflow: db 'Buffer overflow', 0
    err_not_found: db 'Entry not found', 0

global _start

section .text
_start:
    mov rdi, buff
    mov rsi, MAX_LENGTH - 1   
    call read_string
    test rax, rax
    je .error_overflow

    mov rdi, rax
    mov rsi, INDEX
    push rdx
    call find_word
    pop rdx
    test rax, rax
    je .error_not_found

    lea rdi, [rax + rdx + 9]
    call print_string
    xor rdi, rdi
    jmp exit

.error_overflow:
    mov rdi, err_overflow
    call print_error
    mov rdi, 1              
    jmp exit

.error_not_found:
    mov rdi, err_not_found
    call print_error
    mov rdi, 2              
    jmp exit

exit:
    mov rax, 60             
    syscall                 
